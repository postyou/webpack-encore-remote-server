<?php

namespace Postyou;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipStream\Option\Archive;
use ZipStream\ZipStream;
require 'vendor/autoload.php';

class Helper {

    public static function runBuild($type = 'dev', $configName = '') {
        putenv('PATH=' . getenv('PATH') . ':'.ROOTPATH.'node/bin');
        exec("npm run encore " . $type . " --config-name=" . $configName . " 2>&1", $out, $result);
        return [$result, $out];
    }

    public static function getBuildZip($key) {

        $dir = ROOTPATH.'/'.$key.'/build';
        $options = new Archive();
        $options->setSendHttpHeaders(true);

        $zip = new ZipStream(ROOTPATH.'/'.$key.'/build.zip', $options);

        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dir),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file)
        {
            if (!$file->isDir())
            {
                $filePath = $file->getRealPath();
                $zip->addFileFromPath(substr($filePath, strpos($filePath, 'build/') + 6), $filePath);
            }
        }


        return $zip->finish();
    }

    public static function deleteDir($dirPath) {
        $it = new RecursiveDirectoryIterator($dirPath, RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new RecursiveIteratorIterator($it,
            RecursiveIteratorIterator::CHILD_FIRST);
        foreach($files as $file) {
            if ($file->isDir()){
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($dirPath);
    }

    public static function getClientIp() {
        $ipAddress = self::getClientIpEnv();
        if ($ipAddress == 'UNKNOWN') {
            $ipAddress = self::getClientIpServer();
        }
        return $ipAddress;
    }

    private static function getClientIpEnv() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    private static function getClientIpServer() {
    $ipaddress = '';
    if ($_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if($_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if($_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if($_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if($_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if($_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}



}