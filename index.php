<?php



use function GuzzleHttp\Psr7\stream_for;
use Tuupola\Middleware\HttpBasicAuthentication\PdoAuthenticator;
use \OsLab\Slim\MonologProvider;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


require 'vendor/autoload.php';
require './Helper.php';


error_reporting(E_ALL);
//set_error_handler(function ($severity, $message, $file, $line) {
//    if (error_reporting() & $severity) {
//        throw new \ErrorException($message, 0, $severity, $file, $line);
//    }
//});

const ROOTPATH = '/www/htdocs/w01172b0/webpack.postyou.de/';
$GLOBALS['AUTHKEY'] = str_replace('=','',substr($_SERVER['HTTP_AUTHORIZATION'], 6));




//$pathArr = ['/getbuildzip/', '/runwithconfig/','/runwithconfig/[{type}]', '/runwithdefaultconfig/', '/runwithdefaultconfig/[{type}]'];

//$pdo = new PDO("sqlite:/tmp/users.sqlite");
$app = new Slim\App(array(

    'settings' => [
        'debug' => true,
        'displayErrorDetails' => true,
        'logger' => [
            'name' => 'my_logger',
            'handlers' => [
                new StreamHandler(ROOTPATH.'/logs/log.txt', Logger::DEBUG)
            ],
        ],
    ]

));



//$app->add(new RKA\Middleware\IpAddress());
$pdo = new PDO("mysql:host=localhost;dbname=d02f2596", "d02f2596", "k6FPev9w5xsovoxW");
$sth = $pdo->prepare("SELECT tstamp, builds FROM compilecount WHERE username=?");
$sth->execute(array('testuser'));
$dbResult = $sth->fetch(PDO::FETCH_OBJ);



//$pdo->prepare("INSERT INTO accounts (`username`, `password`) VALUES ('testuser',?)")->execute(array(password_hash('testWebpackEncoreRemote', PASSWORD_DEFAULT)));

//if (!isset($_SERVER["REDIRECT_HTTP_AUTHORIZATION"])) {




    $app->add(new Tuupola\Middleware\HttpBasicAuthentication([
            "realm" => "Protected",
            "authenticator" => new PdoAuthenticator([
                "pdo" => $pdo,
                "table" => "accounts",
                "user" => "username",
                "hash" => "password"
            ]),
//            "after" => function ($response, $arguments) {
//                var_dump('tt');
//                exit();
//                return $response->withHeader("X-Brawndo", "plants crave");
//            },
            "before" => function($request, $arguments) use ($dbResult) {
                if ($request->getHeader('PHP_AUTH_USER')[0] == 'testuser') {
                    if ($dbResult->builds > 50) {
                        throw new \Exception('Daily compile limit for test account exceeded');
                    }
                }
            },
        ])
    );



//    $app->add(function ($request, $response, $next) use ($dbResult) {
//
//
//        //            echo '<pre>'. $request->getAttribute('ip_address').'</pre>';
////        exit();
//    });



$c = $app->getContainer();
$c->register(new MonologProvider);

$c['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        return $response->withStatus(500)
            ->withHeader('Content-Type', 'text/html')
            ->withBody(stream_for($exception->getMessage()));
//            ->write($exception->getTraceAsString());
    };
};

$app->get('/', function ($request, $response, $args) {
    return $response->getBody()->write("Hallo, dies ist die Standardseite des Webpackconverters");
});

$app->get('/getbuildzip/', function ($request, $response, $args) {

    return $response->withBody(stream_for(Postyou\Helper::getBuildZip($GLOBALS['AUTHKEY'])));
});

$app->post('/runwithconfig/[{type}]', function ($request, $response, $args) use ($pdo, $dbResult) {

    $uploadedFiles = $request->getUploadedFiles();

    /** Config Section */
    $uploadedFile = $uploadedFiles['config'];
    if (!file_exists(ROOTPATH . $GLOBALS['AUTHKEY'])) {
        mkdir(ROOTPATH . '/'. $GLOBALS['AUTHKEY']);
    }
    $uploadedFile->moveTo(ROOTPATH . '/'. $GLOBALS['AUTHKEY'].'/'.$uploadedFile->getClientFilename());
    $globalConfigStr = file_get_contents(ROOTPATH .'/'.$uploadedFile->getClientFilename());

    $thisConfigStr = trim(file_get_contents(ROOTPATH . '/'. $GLOBALS['AUTHKEY'].'/'.$uploadedFile->getClientFilename()));

    //Remove old config between Comments with key
    $globalConfigStr = trim(preg_replace('/\/\*\*Start'.$GLOBALS['AUTHKEY'].'\*\/([\s\S]*|.*)\/\*\*End'.$GLOBALS['AUTHKEY'].'\*\//m','', $globalConfigStr));
    // Append to new config to global config
    $globalConfigStr = $thisConfigStr.$globalConfigStr;
    // Insert export command if not exists
    if (strpos($globalConfigStr, 'module.exports') === false) {
        $globalConfigStr .= 'module.exports=[]';
    }
    // Remove old entry with key from export array
    $globalConfigStr = preg_replace('/(module\.exports = \[.*?)('.$GLOBALS['AUTHKEY'].',)(.*?\];)/m', '$1$3', $globalConfigStr);
    // Insert new entry
    $globalConfigStr = preg_replace('/(module\.exports = \[.*?)(\];)/m', '$1'.$GLOBALS['AUTHKEY'].',$2', $globalConfigStr);
    // write new global config to file
    $fp = fopen(ROOTPATH .'/'.$uploadedFile->getClientFilename(), 'w');
    fwrite($fp, $globalConfigStr);
    fclose($fp);

    /** Build file Section */
    $uploadedFile = $uploadedFiles['build_files'];

    // Save uploaded zip file and extract
    $uploadedFile->moveTo(ROOTPATH . '/'. $GLOBALS['AUTHKEY'].'/'.$uploadedFile->getClientFilename());
    $zip = new ZipArchive();
    $zip->open(ROOTPATH . '/'. $GLOBALS['AUTHKEY'].'/'.$uploadedFile->getClientFilename());
    $zip->extractTo(ROOTPATH . '/'. $GLOBALS['AUTHKEY'].'/');

    /** Run Build Section */
    $result = Postyou\Helper::runBuild($args['type'], $GLOBALS['AUTHKEY']);
    if ($result[0] !== 0) {
//        $response = $response->withStatus(500)->write(print_r($result[1]));
//        throw new \Exception(print_r($GLOBALS['AUTHKEY']));
        throw new \Exception(print_r($result[1]));
    }

    /** Cleanup Section */
    //TODO: Deletion of directy throws Error in webpack.config.js, cause every configuration is checked
//    Postyou\Helper::deleteDir(ROOTPATH . '/'. $GLOBALS['AUTHKEY']);


    if ($request->getHeader('PHP_AUTH_USER')[0] == 'testuser' ) {

        if ($dbResult->tstamp >= strtotime("today")) {
            $builds = $dbResult->builds += 1;
        } else if ($dbResult->tstamp >= strtotime("yesterday")) {
            $builds = 0;
        }

        $pdo->prepare("UPDATE compilecount SET tstamp=?, builds=? WHERE username=?")->execute(array(time(), $builds, 'testuser'));
    }

    return $response;

});



$app->run();
